package com.akvasoft.rovtwo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class IncidentCountDto implements Serializable {
    private Integer id;
    private String type;
    private Long countNew;
    private Long countAll;
}

package com.akvasoft.rovtwo.utils;

public class DomainConstraints {
    public enum ComplainStatus {
        OPEN, REP_OFFICER, REP_COMPLAINER, SOLVED_OFFICER, SOLVED_COMPLAINER;

        public static ComplainStatus getStatus(String status) {
            if (status != null) {
                return ComplainStatus.valueOf(status);
            }
            return null;
        }
    }

    public enum MessageUserType {
        OFFICER,COMPLAINER;

        public static MessageUserType getStatus(String status) {
            if (status != null) {
                return MessageUserType.valueOf(status);
            }
            return null;
        }
    }

    public enum RoleType {
        OFFICER,COMPLAINER;

        public static RoleType getStatus(String status) {
            if (status != null) {
                return RoleType.valueOf(status);
            }
            return null;
        }
    }

}

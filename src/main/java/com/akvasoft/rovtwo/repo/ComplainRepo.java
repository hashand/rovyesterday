package com.akvasoft.rovtwo.repo;

import com.akvasoft.rovtwo.dto.IncidentCountDto;
import com.akvasoft.rovtwo.entity.Complain;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ComplainRepo extends JpaRepository<Complain,Integer> {

//    @Query(value = "SELECT new com.akvasoft.rovtwo.dto.IncidentCountDto(cc.id, COUNT(c.id) , SUM(IF( c.lastCouncilUser.id IS NULL OR c.lastCouncilUser.id ='' ,1,0))) FROM Complain c LEFT JOIN ComplainCategory cc ON c.category.id=cc.id WHERE c.council.id = ?1 GROUP BY cc.name ")
//    IncidentCountDto getIncidentAllCasesHope(Integer councilId);

//    @Query(value = "SELECT new com.akvasoft.rovtwo.dto.IncidentCountDto(c.category.id, cc.name, COUNT(c.id), COUNT(case c.lastCouncilUser.id  NULL  then 1 else NULL end)) FROM Complain c LEFT JOIN ComplainCategory cc ON c.category.id = cc.id WHERE c.council.id=?1 GROUP BY cc.name")
    @Query(value = "SELECT new com.akvasoft.rovtwo.dto.IncidentCountDto(c.category.id, cc.name, COUNT(c.id), SUM(CASE WHEN (c.lastCouncilUser.id IS NULL OR c.lastCouncilUser.id = NULL) THEN 1 ELSE 0 END)) FROM Complain c LEFT JOIN ComplainCategory cc ON c.category.id = cc.id WHERE c.council.id=?1 GROUP BY cc.name")
    List<IncidentCountDto>  getIncidentAll(Integer councilId);


//    @Query(value = "SELECT t_complain_category.COMPLAIN_CATEGORY_ID, t_complain_category.NAME, COUNT(t_complain.COMPLAIN_ID) , SUM(IF( t_complain.LAST_COUNCIL_USER_ID IS NULL OR t_complain.LAST_COUNCIL_USER_ID ='' ,1,0)) FROM `t_complain` LEFT JOIN t_complain_category ON t_complain.COMPLAIN_CATEGORY_ID=t_complain_category.COMPLAIN_CATEGORY_ID WHERE `URBAN_COUNCIL_ID`=1 GROUP BY t_complain_category.NAME", nativeQuery = true)
//    List getIncidentAllCases(Integer councilId);
//    @Query(value = "SELECT t_complain_category.COMPLAIN_CATEGORY_ID, t_complain_category.NAME, COUNT(t_complain.COMPLAIN_ID) , SUM(IF( t_complain.LAST_COUNCIL_USER_ID IS NULL OR t_complain.LAST_COUNCIL_USER_ID ='' ,1,0)) FROM `t_complain` LEFT JOIN t_complain_category ON t_complain.COMPLAIN_CATEGORY_ID=t_complain_category.COMPLAIN_CATEGORY_ID WHERE `URBAN_COUNCIL_ID`=1 GROUP BY t_complain_category.NAME", nativeQuery = true)
//    List<Object[]> getIncidentAllCases(Integer councilId);


}

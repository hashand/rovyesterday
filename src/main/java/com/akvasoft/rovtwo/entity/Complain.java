package com.akvasoft.rovtwo.entity;

import com.akvasoft.rovtwo.utils.DomainConstraints.ComplainStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Table(name = "t_complain")
public class Complain {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMPLAIN_ID")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "URBAN_COUNCIL_ID",referencedColumnName = "URBAN_COUNCIL_ID")
    private UrbanCouncil council;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LAST_COUNCIL_USER_ID",referencedColumnName = "USER_ID")
    private User lastCouncilUser;

    @Enumerated(EnumType.STRING)
    @Column(name = "STATUS")
    private ComplainStatus status;

    @Column(name = "RESPONSE_TIME_IN_MIN_CEIL")
    private Integer responseTimeInMinCeil;

    @Column(name = "STREET_NAME")
    private String streetName;

    @Column(name = "IMAGE_URLS")
    private String imageUrls;

    @Column(name = "LONGITUDE")
    private Double longitude;

    @Column(name = "LATITUDE")
    private Double latitude;

    @Column(name = "DESCRIPTION")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "COMPLAIN_CATEGORY_ID",referencedColumnName = "COMPLAIN_CATEGORY_ID")
    private ComplainCategory category;

    @Column(name = "CATEGORY_NAME")
    private String categoryName;

    @Column(name = "RATED", columnDefinition = "BIT", length = 1)
    private Boolean rated;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CREATED_BY",referencedColumnName = "USER_ID")
    private User createdBy;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    @Column(name = "SOLVED_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date solvedDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SOLVED_BY",referencedColumnName = "USER_ID")
    private User solvedBy;

}

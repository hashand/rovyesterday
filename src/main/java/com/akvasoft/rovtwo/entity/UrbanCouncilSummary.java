package com.akvasoft.rovtwo.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_urban_council_summary")
public class UrbanCouncilSummary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "URBAN_COUNCIL_SUMMARY_ID")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "DISTRICT_ID",referencedColumnName = "DISTRICT_ID")
    private District district;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "URBAN_COUNCIL_ID",referencedColumnName = "URBAN_COUNCIL_ID")
    private UrbanCouncil urbanCouncil;

    @Column(name = "TOTAL_RECEIVED_COMPLAINTS")
    private Integer totalReceivedComplaints;

    @Column(name = "TOTAL_SOLVED_COUNT")
    private Integer totalSolvedCount;

    @Column(name = "RATING")
    private Double rating;

    @Column(name = "RATING_COUNT")
    private Integer ratingCount;

    @Column(name = "RESPONDED_COMPLAIN_COUNT")
    private Integer respondedComplainCount;

    @Column(name = "AVERAGE_RESPONSE_TIME_IN_MINS")
    private Double averageResponseTimeInMins;

    public UrbanCouncilSummary() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public UrbanCouncil getUrbanCouncil() {
        return urbanCouncil;
    }

    public void setUrbanCouncil(UrbanCouncil urbanCouncil) {
        this.urbanCouncil = urbanCouncil;
    }

    public Integer getTotalReceivedComplaints() {
        return totalReceivedComplaints;
    }

    public void setTotalReceivedComplaints(Integer totalReceivedComplaints) {
        this.totalReceivedComplaints = totalReceivedComplaints;
    }

    public void incrementTotalReceivedComplaints(){
        if(this.totalReceivedComplaints==null){
            this.totalReceivedComplaints=1;
        }else{
            this.totalReceivedComplaints+=1;
        }
    }

    public Integer getTotalSolvedCount() {
        return totalSolvedCount;
    }

    public void setTotalSolvedCount(Integer totalSolvedCount) {
        this.totalSolvedCount = totalSolvedCount;
    }

    public void incrementTotalSolvedCount(){
        if(this.totalSolvedCount==null){
            this.totalSolvedCount=1;
        }else{
            this.totalSolvedCount+=1;
        }
    }

    public void decrementTotalSolvedCount(){
        this.totalSolvedCount-=1;
    }

    public Double getRating() {
        return Objects.requireNonNullElse(this.rating,0.0);
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRatingCount() {
        return Objects.requireNonNullElse(this.ratingCount,0);
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public void incrementRatingCount(){
        if(this.ratingCount==null){
            this.ratingCount=1;
        }else{
            this.ratingCount+=1;
        }
    }

    public Integer getRespondedComplainCount() {
        return Objects.requireNonNullElse(this.respondedComplainCount,0);
    }

    public void incrementRespondedCount(){
        if(this.respondedComplainCount==null){
            this.respondedComplainCount=1;
        }else{
            this.respondedComplainCount+=1;
        }
    }

    public void setRespondedComplainCount(Integer respondedComplainCount) {
        this.respondedComplainCount = respondedComplainCount;
    }

    public Double getAverageResponseTimeInMins() {
        return Objects.requireNonNullElse(averageResponseTimeInMins, 0.0);
    }

    public void setAverageResponseTimeInMins(Double averageResponseTimeInHours) {
        this.averageResponseTimeInMins = averageResponseTimeInHours;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UrbanCouncilSummary{");
        sb.append("id=").append(id);
        sb.append(", district=").append(district);
        sb.append(", urbanCouncil=").append(urbanCouncil);
        sb.append(", totalReceivedComplaints=").append(totalReceivedComplaints);
        sb.append(", totalSolvedCount=").append(totalSolvedCount);
        sb.append(", rating=").append(rating);
        sb.append(", ratingCount=").append(ratingCount);
        sb.append(", respondedComplainCount=").append(respondedComplainCount);
        sb.append(", averageResponseTimeInHours=").append(averageResponseTimeInMins);
        sb.append('}');
        return sb.toString();
    }
}

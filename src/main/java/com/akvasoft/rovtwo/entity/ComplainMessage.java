package com.akvasoft.rovtwo.entity;

import com.akvasoft.rovtwo.utils.DomainConstraints.MessageUserType;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "t_complain_message")
public class ComplainMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMPLAIN_MESSAGE_ID")
    private Integer id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPLAIN_ID",referencedColumnName = "COMPLAIN_ID")
    private Complain complain;

    @Column(name = "MESSAGE")
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COMPLAINED_USER_ID",referencedColumnName = "USER_ID")
    private User complainedUser;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "COUNCIL_MEMBER_ID",referencedColumnName = "USER_ID")
    private User councilMember;

    @Enumerated(EnumType.STRING)
    @Column(name = "USER_TYPE")
    private MessageUserType userType;

    @Column(name = "SMS_SENT", columnDefinition = "BIT", length = 1)
    private Boolean smsSent;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATED_DATE")
    private Date createdDate;

    public ComplainMessage() {
    }



    public MessageUserType getUserType() {
        return userType;
    }

    public void setUserType(MessageUserType userType) {
        this.userType = userType;
    }

    public Integer getId() {
        return id;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Complain getComplain() {
        return complain;
    }

    public void setComplain(Complain complain) {
        this.complain = complain;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getComplainedUser() {
        return complainedUser;
    }

    public void setComplainedUser(User complainedUser) {
        this.complainedUser = complainedUser;
    }

    public User getCouncilMember() {
        return councilMember;
    }

    public void setCouncilMember(User councilMember) {
        this.councilMember = councilMember;
    }

    public Boolean getSmsSent() {
        return smsSent;
    }

    public void setSmsSent(Boolean smsSent) {
        this.smsSent = smsSent;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComplainMessage{");
        sb.append("id=").append(id);
        sb.append(", complain=").append(complain);
        sb.append(", message='").append(message).append('\'');
        sb.append(", complainedUser=").append(complainedUser);
        sb.append(", councilMember=").append(councilMember);
        sb.append(", userType=").append(userType);
        sb.append(", smsSent=").append(smsSent);
        sb.append(", createdDate=").append(createdDate);
        sb.append('}');
        return sb.toString();
    }
}

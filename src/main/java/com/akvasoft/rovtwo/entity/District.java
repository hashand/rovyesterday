package com.akvasoft.rovtwo.entity;


import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
@Table(name = "t_district")
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "DISTRICT_ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "DISTRICT_ID",referencedColumnName = "DISTRICT_ID")
    private List<UrbanCouncil> urbanCouncils;

    public District() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<UrbanCouncil> getUrbanCouncils() {
        return urbanCouncils;
    }

    public void setUrbanCouncils(List<UrbanCouncil> urbanCouncils) {
        this.urbanCouncils = urbanCouncils;
    }
}

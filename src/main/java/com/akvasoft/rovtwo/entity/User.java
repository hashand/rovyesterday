package com.akvasoft.rovtwo.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_user")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "USER_ID")
    private Integer id;

    @Column(name = "NAME")
    private String userName;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "PHONE_NUMBER")
    private String phoneNumber;

    @Column(name = "PHONE_VERIFIED")
    private Boolean phoneVerified;

    @Column(name = "EMAIL")
    private String email;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "URBAN_COUNCIL_ID")
    private UrbanCouncil urbanCouncil;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "DISTRICT_ID")
    private District district;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ROLE_ID")
    private Role role;

    @Column(name = "RATING")
    private Double rating;

    @Column(name = "RATING_COUNT")
    private Integer ratingCount;

    public User() {
    }

    public Integer getId() {
        return id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Boolean getPhoneVerified() {
        return phoneVerified;
    }

    public void setPhoneVerified(Boolean phoneVerified) {
        this.phoneVerified = phoneVerified;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UrbanCouncil getUrbanCouncil() {
        return urbanCouncil;
    }

    public void setUrbanCouncil(UrbanCouncil urbanCouncil) {
        this.urbanCouncil = urbanCouncil;
    }

    public District getDistrict() {
        return district;
    }

    public void setDistrict(District district) {
        this.district = district;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Double getRating() {
        if(rating==null){
            return 0.0;
        }
        return rating;
    }

    public void setRating(Double rating) {
        this.rating = rating;
    }

    public Integer getRatingCount() {
        if(ratingCount==null){
            return 0;
        }
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public void incrementRatingCount() {
        if(this.ratingCount==null){
            this.ratingCount=0;
        }
        this.ratingCount+=1;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("User{");
        sb.append("id=").append(id);
        sb.append(", userName='").append(userName).append('\'');
        sb.append(", password='").append(password).append('\'');
        sb.append(", phoneNumber='").append(phoneNumber).append('\'');
        sb.append(", phoneVerified=").append(phoneVerified);
        sb.append(", email='").append(email).append('\'');
        sb.append(", urbanCouncil=").append(urbanCouncil);
        sb.append(", district=").append(district);
        sb.append(", role=").append(role);
        sb.append(", rating=").append(rating);
        sb.append(", ratingCount=").append(ratingCount);
        sb.append('}');
        return sb.toString();
    }
}


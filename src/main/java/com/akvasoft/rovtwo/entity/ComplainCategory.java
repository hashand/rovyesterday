package com.akvasoft.rovtwo.entity;


import javax.persistence.*;

@Entity
@Table(name = "t_complain_category")
public class ComplainCategory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "COMPLAIN_CATEGORY_ID")
    private Integer id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "MAX_WAIT_DAYS")
    private Integer maxWaitDays;

    public ComplainCategory() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getMaxWaitDays() {
        return maxWaitDays;
    }

    public void setMaxWaitDays(Integer maxWaitDays) {
        this.maxWaitDays = maxWaitDays;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("ComplainCategory{");
        sb.append("id=").append(id);
        sb.append(", name='").append(name).append('\'');
        sb.append(", maxWaitDays=").append(maxWaitDays);
        sb.append('}');
        return sb.toString();
    }
}

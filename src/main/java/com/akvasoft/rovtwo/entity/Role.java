package com.akvasoft.rovtwo.entity;
import com.akvasoft.rovtwo.utils.DomainConstraints.RoleType;

import javax.persistence.*;

@Entity
@Table(name = "t_role")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ROLE_ID")
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(name = "ROLE")
    private RoleType role;

    public Role() {
    }

    public Role(Integer id, RoleType roleType) {
        this.id = id;
        this.role = roleType;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public RoleType getRole() {
        return role;
    }

    public void setRole(RoleType role) {
        this.role = role;
    }
}

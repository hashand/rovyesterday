package com.akvasoft.rovtwo.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_urban_council")
public class UrbanCouncil {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "URBAN_COUNCIL_ID")
    private Integer id;

    @Column(name = "DISTRICT_ID")
    private Integer districtId;

    @Column(name = "NAME")
    private String name;

    @Column(name = "LEFT_LONGITUDE")
    private Double leftLongitude;

    @Column(name = "RIGHT_LONGITUDE")
    private Double rightLongitude;

    @Column(name = "TOP_LATITUDE")
    private Double topLatitude;

    @Column(name = "BOTTOM_LATITUDE")
    private Double bottomLatitude;

    public UrbanCouncil() {
    }


    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer district) {
        this.districtId = district;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getLeftLongitude() {
        return leftLongitude;
    }

    public void setLeftLongitude(Double leftLongitude) {
        this.leftLongitude = leftLongitude;
    }

    public Double getRightLongitude() {
        return rightLongitude;
    }

    public void setRightLongitude(Double rightLongitude) {
        this.rightLongitude = rightLongitude;
    }

    public Double getTopLatitude() {
        return topLatitude;
    }

    public void setTopLatitude(Double topLatitude) {
        this.topLatitude = topLatitude;
    }

    public Double getBottomLatitude() {
        return bottomLatitude;
    }

    public void setBottomLatitude(Double bottomLatitude) {
        this.bottomLatitude = bottomLatitude;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("UrbanCouncil{");
        sb.append("id=").append(id);
        sb.append(", district=").append(districtId);
        sb.append(", name='").append(name).append('\'');
        sb.append(", leftLongitude=").append(leftLongitude);
        sb.append(", rightLongitude=").append(rightLongitude);
        sb.append(", topLatitude=").append(topLatitude);
        sb.append(", bottomLatitude=").append(bottomLatitude);
        sb.append('}');
        return sb.toString();
    }
}

package com.akvasoft.rovtwo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication
public class RovtwoApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		SpringApplication.run(RovtwoApplication.class, args);
		System.out.println("\ncheck >> localhost:8084/api");
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(RovtwoApplication.class);
	}


}

package com.akvasoft.rovtwo.controller;

import com.akvasoft.rovtwo.service.IncidentCountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping
public class IncidentCountsController {

    @Autowired
    private IncidentCountService incidentCountService;

    @GetMapping("/hello")
    public String test(){
        return "hello";
    }

    @GetMapping("/test/{councilId}")
    public ResponseEntity retrieveForAdmin(@PathVariable(value = "councilId") Integer id){
        return new ResponseEntity(incidentCountService.getAllAndNewCount(id), HttpStatus.OK);
    }
}

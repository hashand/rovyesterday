package com.akvasoft.rovtwo.service;

import com.akvasoft.rovtwo.dto.IncidentCountDto;
import com.akvasoft.rovtwo.repo.ComplainRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class IncidentCountService {

    @Autowired
    private ComplainRepo complainRepo;

    @Transactional(propagation = Propagation.SUPPORTS)
    public List<IncidentCountDto> getAllAndNewCount(Integer urbanCouncilId){
        //List<IncidentCountDto> incidentCountDto =complainRepo.getIncidentAllCases(urbanCouncilId);
//        HashMap<String,Object> = new HashMap<String,O>()
//        List<Object[]> objects = complainRepo.getIncidentAllCases(urbanCouncilId);
//
////        for(Object[] obj : objects){
////            System.out.println(obj[1]);
////        }

    return complainRepo.getIncidentAll(urbanCouncilId);
        //return incidentCountDto.stream().collect(Collectors.toList());

        //return voluntaryOrganizations.stream().map(VoluntaryOrganization::getVoluntaryOrganizationDto).collect(Collectors.toList());
    }

}
